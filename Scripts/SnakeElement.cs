using Godot;
using System;
using System.Collections.Generic;
using static Utils;

public class SnakeElement : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    [Export] private int StartingLength = 3;
    [Export] private Color SnakeBodyColor = Color.ColorN("lightgreen");
    [Export] private Color SnakeHeadColor = Color.ColorN("lightblue");

    [Signal] delegate void ScoreChanged(int score);
    [Signal] delegate void GameOver(string reason, int score);
    [Signal] delegate void NewGame();

    const int AppleCollisionLayerBit = 1;
    const int SnakeCollisionLayerBit = 0;
    const int WallCollisionLayerBit = 2;

    private Vector2 _direction;
    private Vector2 _startingPosition;
    private LinkedList<Vector2> _nextDirections;
    private PackedScene _snakeElementScene;
    private int _numberOfSnakeElementsToSpawn;
    private int _score;

    public override void _Ready()
    {
        _score = 0;
        _direction = Vector2.Right;
        _nextDirections = new LinkedList<Vector2>();
        _snakeElementScene = GD.Load<PackedScene>("res://Scenes/SnakeElement.tscn");
        _startingPosition = this.GetViewportCenterSnap();
        _numberOfSnakeElementsToSpawn = StartingLength;
        
        EmitSignal(nameof(NewGame));
        EmitSignal(nameof(ScoreChanged), 0);
    }

    public override void _Process(float delta)
    {
        if(Input.IsActionJustPressed("ui_up")){
            TryChangeDirection(Vector2.Up);
        }
        else if(Input.IsActionJustPressed("ui_left")){
            TryChangeDirection(Vector2.Left);
        }
        else if(Input.IsActionJustPressed("ui_right")){
            TryChangeDirection(Vector2.Right);
        }
        else if(Input.IsActionJustPressed("ui_down")){
            TryChangeDirection(Vector2.Down);
        }

        #if DEBUG
        if(Input.IsActionJustPressed("ui_accept")) {
            Spawn();
        }
        
        if(Input.IsActionJustPressed("ui_x")) {
            EmitSignal(nameof(ScoreChanged), ++_score);
        }

        if(Input.IsActionJustPressed("ui_d")) {
            EmitSignal(nameof(GameOver), "debug", _score);
        }
        #endif
        
    }

    private void TryChangeDirection(Vector2 newDirection)
    {
        var currentDirection = _nextDirections.Last?.Value ?? _direction;

        var newDirectionUnit = newDirection.Normalized();
        if(newDirectionUnit.Dot(currentDirection) == 0) {
            _nextDirections.AddLast(newDirectionUnit);
        }
    }

    private void UpdateDirection(){
        if(_nextDirections.First is LinkedListNode<Vector2> first){
            _direction = first.Value;
            _nextDirections.RemoveFirst();
        }
    }

    private Node2D GetHead()
    {
        var childCount = GetChildCount();
        if(childCount > 0)
            return GetChild<Node2D>(0);
        return null;
    }

    private Node2D GetTail()
    {
        var childCount = GetChildCount();
        if(childCount > 1)
            return GetChild<Node2D>(childCount - 1);
        return null;
    }

    private void Move(){
        Vector2? previousElementPosition = null;
        Vector2? currentElementPosition;

        for(var index = 0; index < GetChildCount(); index++){
            var element = GetChild<Node2D>(index);
            
            currentElementPosition = new Vector2(element.GlobalPosition);

            if(index == 0) {
                var displacement = TileSize * _direction; 
                TestCollision((KinematicBody2D) element, displacement);
                TranslateWithPacmanRules(element, displacement);
            } else {
                element.GlobalPosition = previousElementPosition.Value;
            }

            previousElementPosition = currentElementPosition;
        }
    }

    private void TranslateWithPacmanRules(Node2D body2D, Vector2 displacement)
    {
        var viewport = this.GetViewportSizeSnap();
        var newPosition = body2D.GlobalPosition + displacement;

        if(newPosition.x >= viewport.x) {
            newPosition.x = 0;
        }

        if(newPosition.y >= viewport.y) {
            newPosition.y = 0;
        }

        if(newPosition.x < 0) {
            newPosition.x = viewport.x - TileSize;
        }

        if(newPosition.y < 0) {
            newPosition.y = viewport.y - TileSize;
        }

        body2D.GlobalPosition = newPosition;
    }

    private void TestCollision(KinematicBody2D kinematicBody, Vector2 displacement){
        var collision = kinematicBody.MoveAndCollide(displacement, testOnly: true);

        if(collision?.Collider is PhysicsBody2D collider) {
            if (collider.GetCollisionLayerBit(AppleCollisionLayerBit)){
                AppleCollisionEventHandler();
            }

            if (collider.GetCollisionLayerBit(SnakeCollisionLayerBit)){
                SnakeCollisionEventHandler();
            }

            if (collider.GetCollisionLayerBit(WallCollisionLayerBit)){
                WallCollisionEventHandler();
            }
        }
    }

    private void AppleCollisionEventHandler()
    {
        GD.Print("Collided with apple, yum!");
        
        EmitSignal(nameof(ScoreChanged), ++_score);
        
        Spawn();
    }

    private void SnakeCollisionEventHandler()
    {
        GD.Print("Collided with self, die!");
        EmitSignal(nameof(GameOver), "You ate yourself", ++_score);
    }

    private void WallCollisionEventHandler()
    {
        GD.Print("Collided with wall, die!");
        EmitSignal(nameof(GameOver), "You crashed onto a wall", ++_score);
    }

    private Node2D GenerateSnakeElement(Color color){
        var node = _snakeElementScene.Instance<KinematicBody2D>();
        node.GlobalPosition = GetTail()?.GlobalPosition ?? _startingPosition;

        if (color != null) {
            node.GetNode<Polygon2D>("Polygon2D").Color = color;
        }

        return node;
    }

    private void Spawn() {
            var color = GetChildCount() == 0 ? SnakeHeadColor : SnakeBodyColor;
            var child = GenerateSnakeElement(color);
            CallDeferred("add_child", child);
    }

    public void GameTimerTimeoutEventHandler(){
        if(_numberOfSnakeElementsToSpawn-- > 0){
            Spawn();
        }
        UpdateDirection();
        Move();
    }

    private void GameOverDialogConfirmedEventHandler()
    {      
        foreach(Node child in GetChildren()){
            child.Free();
        }

        _Ready();
    }
}
