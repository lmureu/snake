using Godot;
using System;

public class GameTimer : Timer
{
    [Export] float InitialInterval = 0.2f;
    [Export] float LevelDeltaInterval = -0.02f;
    [Export] float MinInterval = 0.08f;
    [Export] int LevelUpScore = 10;

    public void NewGameEventHandler()
    {
        WaitTime = InitialInterval;
    }

    public void ScoreChangedEventHandler(int score)
    {
        if(score % LevelUpScore == 0) {
            WaitTime = Mathf.Clamp(WaitTime+LevelDeltaInterval, MinInterval, InitialInterval);
            GD.Print("WaitTime: ", WaitTime);
        }
    }
}
