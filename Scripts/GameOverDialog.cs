using Godot;
using System;

public class GameOverDialog : AcceptDialog
{
    public void GameOverEventHandler(string reason, int score)
    {
        DialogText = $"{reason}!\nScore: {score}";
        SetAsMinsize();
        PopupCenteredMinsize();
    }
}
