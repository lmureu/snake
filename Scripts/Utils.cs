using Godot;
using System;

public static class Utils
{
    public static int TileSize => (int)ProjectSettings.GetSetting("global/tile_size");

    public static float Snap(float value)
    {
        return Mathf.Floor(value / TileSize) * TileSize;
    }

    public static Vector2 GetViewportSizeSnap(this Node2D node)
    {
        var viewport = node.GetViewportRect();
        var size = new Vector2()
        {
            x = Snap(viewport.Size.x),
            y = Snap(viewport.Size.y)
        };
        return size;
    }

    public static Vector2 GetViewportCenterSnap(this Node2D node)
    {
        return node.GetViewportSizeSnap() / 2;
    }

    public static uint RandInt(uint min, uint max)
    {
        return (GD.Randi() % (max - min)) + min;
    }
}