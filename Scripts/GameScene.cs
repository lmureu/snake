using Godot;
using System;

using NLog;
using NLog.Config;
using NLog.Targets;
using PostSharp.Patterns.Diagnostics;
using PostSharp.Patterns.Diagnostics.Backends.NLog;

public class GameScene : Node2D
{
    #if DEBUG
    public override void _EnterTree()
    {        
        // Configure NLog.
        var nlogConfig = new LoggingConfiguration();

        var fileTarget = new FileTarget("file")
        {
            FileName = "nlog.log",
            KeepFileOpen = true,
            ConcurrentWrites = false,
        };

        nlogConfig.AddTarget(fileTarget);
        nlogConfig.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, fileTarget));

        var consoleTarget = new ConsoleTarget("console");
        nlogConfig.AddTarget(consoleTarget);
        nlogConfig.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, consoleTarget));

        LogManager.EnableLogging();

        // Configure PostSharp Logging to use NLog.
        LoggingServices.DefaultBackend = new NLogLoggingBackend(new LogFactory(nlogConfig));

        GD.Print("Enabled logging");
    }
    #endif
}
