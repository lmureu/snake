using Godot;
using System;
using static Utils;

public class Apple : KinematicBody2D
{

    private int _ticks;
    [Export] private int ChangePositionTickCount = 15;

    public void ChooseRandomPosition()
    {
        var viewportTileCount = this.GetViewportSizeSnap() / TileSize;

        var position = new Vector2();

        GD.Print("Begin ChooseRandomPosition loop");
        do
        {
            position.x = RandInt(0, (uint) viewportTileCount.x);
            position.y = RandInt(0, (uint) viewportTileCount.y);
            position *= TileSize;
        } while (CheckTileIsOccupied(position));
        GD.Print("End ChooseRandomPosition loop");

        GlobalPosition = position;
        _ticks = 0;
    }

    public bool CheckTileIsOccupied(Vector2 position)
    {
        /*GlobalPosition = position;
        var collision = MoveAndCollide(new Vector2(), testOnly: true);
        var hasCollided = collision?.Collider is PhysicsBody2D;
        return hasCollided;*/
        return TestMove(new Transform2D(0, position), Vector2.Zero);
    }

    public void GameTimerTimeoutEventHandler()
    {
        if (++_ticks == ChangePositionTickCount)
        {
            ChooseRandomPosition();
        }
    }

    public void AppleChangePositionEventHandler() => ChooseRandomPosition();
    public void NewGameEventHandler() => ChooseRandomPosition();
    public void ScoreChangedEventHandler(int score) => ChooseRandomPosition();
}
