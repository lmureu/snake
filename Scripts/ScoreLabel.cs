using Godot;
using System;

public class ScoreLabel : Label
{
    public void ScoreChangedEventHandler(int score){
        Text = $"Score: {score}";
    }
}
