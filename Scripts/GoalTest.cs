using Godot;
using System;
using static Utils;

public class GoalTest : KinematicBody2D
{
    public override void _Ready()
    {
    }

    public override void _Process(float delta)
    {
        if (Input.IsActionJustPressed("ui_accept"))
        {
            ChooseRandomPosition();
        }
    }

    public bool CheckTileIsOccupied(Vector2 position)
    {
        /*GlobalPosition = position;
        var collision = MoveAndCollide(new Vector2(), testOnly: true);
        if(collision?.Collider is PhysicsBody2D collider) {
            GD.PrintS("collider", collider.Name);
            return true;
        }
        return false;*/

        return TestMove(new Transform2D(0, position), Vector2.Zero);
    }

    public void ChooseRandomPosition()
    {
        var viewportTileCount = new Vector2(4, 4);

        GD.PrintS("Viewport:", viewportTileCount);

        for (var x = 0; x < viewportTileCount.x; x++)
        {
            for (var y = 0; y < viewportTileCount.y; y++)
            {
                var position = new Vector2(x, y);
                var msg = CheckTileIsOccupied(position * TileSize) ? "occupied" : "free";
                GD.PrintS("position", position, msg);
            }
        }
    }
}
