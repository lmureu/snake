using Godot;
using System;

public class PauseDialog : AcceptDialog
{
    private bool _paused = false;
    private bool _dialog = false;

    public override void _Process(float delta){
        if(_dialog) return;

        if(Input.IsActionJustPressed("ui_cancel")){
            if(_paused) {
                _paused = false;
            } else {
                SetAsMinsize();
                PopupCenteredMinsize();
                Pause();
            }
        }
    }

    public void GameOverEventHandler(string reason, int score)
    {
        _dialog = true;
        Pause();
    }

    public void GameOverDialogConfirmedEventHandler() {
        _dialog = false;
        Unpause();
    }

    public void HideEventHandler() {
        GD.Print("hide");
        Unpause();
    }

    private void Pause()
    {
        GD.Print("pause");
        _paused = true;
        GetTree().Paused = true;
    }

    private void Unpause() {
        GD.Print("unpause");
        GetTree().Paused = false;
    }
}
